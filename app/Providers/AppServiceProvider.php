<?php

namespace App\Providers;

use App\Services\MessageQueue\MessageQueueInterface;
use App\Services\MessageQueue\RabbitMQService;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use PhpAmqpLib\Connection\AMQPStreamConnection;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
      MessageQueueInterface::class => RabbitMQService::class
    ];

    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->singleton(AMQPStreamConnection::class, function (Application $app) {
            return new AMQPStreamConnection(
                config('queue.connections.rabbitmq.host'),
                config('queue.connections.rabbitmq.port'),
                config('queue.connections.rabbitmq.user'),
                config('queue.connections.rabbitmq.password'),
                config('queue.connections.rabbitmq.vhost')
            );
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
