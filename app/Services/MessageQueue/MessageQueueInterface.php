<?php

namespace App\Services\MessageQueue;

use App\Services\MessageQueue\DTO\MessageDTO;

interface MessageQueueInterface
{
    public function publish(MessageDTO $messageDTO): void;
    public function consume(): void;
}
