<?php

namespace App\Services\MessageQueue;

use App\Services\MessageQueue\DTO\MessageDTO;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQService implements MessageQueueInterface
{
    private const EXCHANGE_TITLE = 'chirps_exchange';
    private const QUEUE_TITLE = 'chirps_queue';
    private const ROUTING_KEY = 'chirps_key';

    public function __construct(
        private AMQPStreamConnection $connection
    )
    {
        //
    }

    /**
     * @throws \Exception
     */
    public function publish(MessageDTO $messageDTO): void
    {
        $channel = $this->connection->channel();
        $channel->exchange_declare(
            self::EXCHANGE_TITLE, 'direct', false, false, false
        );
        $channel->queue_declare(self::QUEUE_TITLE, false, false, false, false);
        $channel->queue_bind(self::QUEUE_TITLE, self::EXCHANGE_TITLE, self::ROUTING_KEY);

        $msgBody = 'User ' . $messageDTO->username . ' sent a chirp: ' . $messageDTO->chirp;

        $msg = new AMQPMessage($msgBody);
        $channel->basic_publish($msg, self::EXCHANGE_TITLE, self::ROUTING_KEY);
        echo " [x] Sent $msgBody to " . self::EXCHANGE_TITLE . " / " . self::QUEUE_TITLE . ".\n";
        $channel->close();
        $this->connection->close();
    }

    /**
     * @throws \Exception
     */
    public function consume(): void
    {
        $channel = $this->connection->channel();
        $callback = function ($msg) {
            echo ' [x] ', $msg->body, "\n";
        };
        $channel->queue_declare(self::QUEUE_TITLE, false, false, false, false);
        $channel->basic_consume(
            self::QUEUE_TITLE, '', false, true, false, false, $callback
        );
        echo 'Waiting for new message on ' . self::QUEUE_TITLE, " \n";
        while ($channel->is_consuming()) {
            $channel->wait();
        }
        $channel->close();
        $this->connection->close();
    }
}
