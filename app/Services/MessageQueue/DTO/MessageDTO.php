<?php

namespace App\Services\MessageQueue\DTO;

class MessageDTO
{
    public function __construct(public readonly string $username, public readonly string $chirp)
    {
        //
    }
}
